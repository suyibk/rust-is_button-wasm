A.rust构建wasm

1.构建项目

cargo new --lib hello-wasm

2.示例代码src/lib.rs

```
    extern crate wasm_bindgen;
    use wasm_bindgen::prelude::*;
    #[wasm_bindgen]
    extern {
        //在 Rust 中调用来自 JavaScript 的外部函数
        pub fn alert(s: &str);
    }
    //提供外面调用方法
    #[wasm_bindgen]
    pub fn greet(name: &str) {
        alert(&format!("Hello, {}!", name));
    }
```

3.打包方式

    打成可以直接调用方式

        wasm-pack build --target web

    打包并推送node仓库

        wasm-pack build --scope 仓库名称

B.使用wasm

1.配置webpack.config.js (此示例添加到的配置webpack.common.js)

2.在 Webpack5 中，resolve.extensions 默认值为 ['.js', '.json', '.wasm']

```
    module.exports = {
    //...
    experiments: {
        asyncWebAssembly: true,
    },
    resolve: {
        extensions: ['.wasm'...]
    }
    //...
    }
```

3.这里用的是直接使用的方式将rust打包后的pkg的文件复制到==》wasm文件所在 src/utils/is_button-wasm

4.具体引用和调用

```
import init, { greet } from './is_button-wasm/is_button_wasm'
export async function run_wasm(str) {
    await init()
    greet(str)
}
```


