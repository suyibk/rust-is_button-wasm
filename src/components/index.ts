
import '../assets/icons/index'
// @ts-ignore
import SvgIcon from './SvgIcon'
// @ts-ignore
import CForm from './CForm'
// @ts-ignore
import CMenuCom from './CMenuCom'
// @ts-ignore
import CTitle from './CTitle'
// import ShowPdf from './ShowPdf'// 表单组件
// register globally
import { camelToUnderline } from '@/utils'
export const registerCom = (app: { component: (arg0: any, arg1: any) => any }) => {
  const comList = [SvgIcon, CForm, CMenuCom, CTitle]
  comList.forEach(async item => {
    const name = camelToUnderline(item.name)
    await app.component(name, item)
  })
}
