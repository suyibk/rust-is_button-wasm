import { createApp } from 'vue'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

// @ts-ignore
import App from './App' // 引入 APP 页面组建
const app = createApp(App) // 通过 createApp 初始化 app

// import { registerEleCom } from './lib/element-plus'// 按需引入element-plus组件
// registerEleCom(app)
app.use(ElementPlus, { size: 'small', zIndex: 3000 })

import './assets/icons/index'
// @ts-ignore
import SvgIcon from './components/SvgIcon'
// @ts-ignore
import CForm from './components/CForm'
// @ts-ignore
import CMenuCom from './components/CMenuCom'
// @ts-ignore
import CTitle from './components/CTitle'
// import ShowPdf from './ShowPdf'// 表单组件
// register globally
import { camelToUnderline } from '@/utils'
export const registerCom = (app: { component: (arg0: any, arg1: any) => any }) => {
    const comList = [SvgIcon, CForm, CMenuCom, CTitle]
    comList.forEach(async item => {
        const name = camelToUnderline(item.name)
        await app.component(name, item)
    })
}


// 引入全局组件
// @ts-ignore
registerCom(app)

import router from './router/index'
app.use(router)

import store from './store'
app.use(store)

app.mount('#root') // 将页面挂载到 root 节点
